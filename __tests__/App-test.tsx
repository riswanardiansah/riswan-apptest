import React from 'react';
import App from '../src/app/App';
import {render} from '@testing-library/react-native';

test('Should render App', () => {
  const rendere = render(<App />).toJSON();
  expect(rendere).toMatchSnapshot();
});
