import React from 'react';
import DetailScreen from '../src/features/Details';
import {render, cleanup} from '@testing-library/react-native';
import {Provider as StoreProvider} from 'react-redux';
import {store} from '../src/app/store';
import {NavigationContainer} from '@react-navigation/native';
import {Provider as PaperProvider} from 'react-native-paper';

afterEach(cleanup);

describe('DetailScreen', () => {
  const navigation = {navigate: jest.fn()};
  it('Should render Detail Screen', () => {
    const rendere = render(
      <StoreProvider store={store}>
        <PaperProvider>
          <NavigationContainer>
            <DetailScreen
              navigation={navigation}
              route={{params: {userID: '93ad6070-c92b-11e8-b02f-cbfa15db428b'}}}
            />
          </NavigationContainer>
        </PaperProvider>
      </StoreProvider>,
    ).toJSON();
    expect(rendere).toMatchSnapshot();
  });

  it('Get Detail Contact', () => {
    const rendered = render(
      <StoreProvider store={store}>
        <PaperProvider>
          <NavigationContainer>
            <DetailScreen
              navigation={navigation}
              route={{params: {userID: '93ad6070-c92b-11e8-b02f-cbfa15db428b'}}}
            />
          </NavigationContainer>
        </PaperProvider>
      </StoreProvider>,
    );
    const cellInputName = rendered.getByTestId('firstName-detail');
    const cellLastName = rendered.getByTestId('lastName-detail');
    const cellAge = rendered.getByTestId('age-detail');
    expect(cellInputName).toBeDefined();
    expect(cellLastName).toBeDefined();
    expect(cellAge).toBeDefined();
  });
});
