import React from 'react';
import HomeScreen from '../src/features/Home';
import Create from '../src/features/Create';
import {render, cleanup, fireEvent} from '@testing-library/react-native';
import {Provider as StoreProvider} from 'react-redux';
import {store} from '../src/app/store';
import {NavigationContainer} from '@react-navigation/native';
import Provider from './Provider';
import {Provider as PaperProvider} from 'react-native-paper';

afterEach(cleanup);

const createProps = () => ({
  navigation: {
    navigate: jest.fn(),
  },
});
describe('HomeScreen', () => {
  it('Render HomeScreen', () => {
    const rendered = render(
      <Provider>
        <NavigationContainer>
          <HomeScreen {...createProps()} />
        </NavigationContainer>
      </Provider>,
    ).toJSON();
    expect(rendered).toBeDefined();
  });

  it('Flatlist should be visible', () => {
    const navigation = {navigate: jest.fn()};

    const rendered = render(
      <StoreProvider store={store}>
        <NavigationContainer>
          <HomeScreen navigation={navigation} />
        </NavigationContainer>
      </StoreProvider>,
    );
    const flatlist = rendered.getByTestId('flatlist-contact');
    expect(flatlist).toBeDefined();
  });

  it('Should navigate between Screen', () => {
    const navigation = {navigate: jest.fn()};

    const rendered = render(
      <StoreProvider store={store}>
        <NavigationContainer>
          <HomeScreen navigation={navigation} />
        </NavigationContainer>
      </StoreProvider>,
    );
    const btn = rendered.getByTestId('add-btn');
    expect(btn).toBeDefined();
    fireEvent.press(btn);
    const renderedCreate = render(
      <StoreProvider store={store}>
        <PaperProvider>
          <NavigationContainer>
            <Create
              navigation={navigation}
              route={{params: {mode: 'CREATE'}}}
            />
          </NavigationContainer>
        </PaperProvider>
      </StoreProvider>,
    ).toJSON();
    expect(renderedCreate).toBeDefined();
  });
});
