import {store} from '../src/app/store';
import fetchMock from 'fetch-mock';

const apiGetContact = 'https://simple-contact-crud.herokuapp.com/contact';

it('Should initially set contacts to an empty array of object', () => {
  const state = store.getState().contacts;
  expect(state.contacts).toEqual([]);
});

it('Get contact should not empty ', async () => {
  fetchMock.mock(apiGetContact, 200);
  const res = await fetch(apiGetContact);
  expect(res.ok).toEqual(true);
  fetchMock.restore();
});

const contactID = {
  id: '93ad6070-c92b-11e8-b02f-cbfa15db428b',
  firstName: 'Bilbo',
  lastName: 'Baggins',
  age: 111,
  photo:
    'http://vignette1.wikia.nocookie.net/lotr/images/6/68/Bilbo_baggins.jpg/revision/latest?cb=20130202022550',
};

const editedContactID = {
  ...contactID,
  firstName: 'Red',
};

it('Get contact by ID ', async () => {
  fetchMock.mock(`${apiGetContact}/93ad6070-c92b-11e8-b02f-cbfa15db428b`, 200);
  fetch(`${apiGetContact}/93ad6070-c92b-11e8-b02f-cbfa15db428b`, {
    method: 'GET',
  }).then(response =>
    response.json().then(item => {
      expect(item).toEqual(contactID);
    }),
  );
});
it('Update contact by ID ', async () => {
  fetchMock.mock(`${apiGetContact}/93ad6070-c92b-11e8-b02f-cbfa15db428b`, 200, {
    overwriteRoutes: true,
  });
  fetch(`${apiGetContact}/93ad6070-c92b-11e8-b02f-cbfa15db428b`, {
    method: 'PUT',
    body: JSON.stringify(editedContactID),
  }).then(response =>
    response.json().then(item => {
      expect(item).toEqual(editedContactID);
    }),
  );
});
