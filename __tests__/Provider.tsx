import React, {PropsWithChildren} from 'react';
import {Provider as StoreProvider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {store, persistor} from '../src/app/store';
import {render} from '@testing-library/react-native';

const Provider = ({children}: PropsWithChildren) => {
  return (
    <StoreProvider store={store}>
      <PersistGate persistor={persistor}>{children}</PersistGate>
    </StoreProvider>
  );
};

export default Provider;

test('Render Provider', () => {
  render(<Provider />);
});
