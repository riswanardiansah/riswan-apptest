import React from 'react';
import CreateScreen from '../src/features/Create';
import {render, cleanup} from '@testing-library/react-native';
import {Provider as StoreProvider} from 'react-redux';
import {store} from '../src/app/store';
import {NavigationContainer} from '@react-navigation/native';
import {Provider as PaperProvider} from 'react-native-paper';
afterEach(cleanup);
describe('CreateScreen', () => {
  const navigation = {navigate: jest.fn()};
  it('Should render Create Edit Screen', async () => {
    const rendered = render(
      <StoreProvider store={store}>
        <PaperProvider>
          <NavigationContainer>
            <CreateScreen
              navigation={navigation}
              route={{params: {mode: 'CREATE'}}}
            />
          </NavigationContainer>
        </PaperProvider>
      </StoreProvider>,
    ).toJSON();
    expect(rendered).toMatchSnapshot();
  });

  it('Test Input', () => {
    const rendered = render(
      <StoreProvider store={store}>
        <PaperProvider>
          <NavigationContainer>
            <CreateScreen
              navigation={navigation}
              route={{params: {mode: 'CREATE'}}}
            />
          </NavigationContainer>
        </PaperProvider>
      </StoreProvider>,
    );
    const textInputName = rendered.getByTestId('firstName');
    const textLastName = rendered.getByTestId('lastName');
    const textAge = rendered.getByTestId('age');
    expect(textInputName).toBeDefined();
    expect(textLastName).toBeDefined();
    expect(textAge).toBeDefined();
  });
});
