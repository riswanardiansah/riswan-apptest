import React, {useCallback} from 'react';
import {useFocusEffect} from '@react-navigation/native';

import {View, FlatList, Image} from 'react-native';
import {NavigationProps} from 'common/interface';
import {useAppDispatch, useTypedSelector} from '../../common/hooks/';
import {getContactAction} from '../../common/reducers/contact';
import {ContactState} from 'common/models';
import {FAB, List} from 'react-native-paper';
import {Container} from '../../common/components';
import styles from './style';
import {validateImageUri} from '../../common/utils';

type HomeScreenProps = NavigationProps<'Details'>;

const HomeScreen = ({navigation}: HomeScreenProps) => {
  const {contacts} = useTypedSelector<ContactState>('contacts');
  const getContact = useAppDispatch(getContactAction);

  useFocusEffect(
    useCallback(() => {
      getContact();
    }, [getContact]),
  );

  const ItemSeparator = () => {
    return <View style={styles.separator} />;
  };

  const onItemPress = useCallback(
    (id: string) => {
      navigation.navigate('Details', {
        userID: id,
      });
    },
    [navigation],
  );
  const onCreatePress = useCallback(() => {
    let timeout;
    if (timeout) {
      clearTimeout(timeout);
    }
    timeout = setTimeout(() => {
      navigation.navigate('Create', {mode: 'CREATE'});
    }, 500);
  }, [navigation]);

  const renderImgUri = useCallback((uri?: string) => {
    return validateImageUri(uri);
  }, []);
  return (
    <Container>
      <FlatList
        data={contacts}
        renderItem={({item, index}) => {
          const isOdd = index % 2 === 1;
          return (
            <List.Item
              title={`${item.firstName} ${item.lastName}`}
              onPress={() => onItemPress(item.id)}
              style={isOdd ? styles.odd : styles.even}
              left={() => (
                <Image
                  source={
                    renderImgUri(item.photo)
                      ? {uri: item.photo}
                      : require('../../common/assests/images/avatar.png')
                  }
                  style={styles.avatar}
                  resizeMode="cover"
                />
              )}
            />
          );
        }}
        ItemSeparatorComponent={ItemSeparator}
        keyExtractor={item => item.id}
        testID="flatlist-contact"
      />
      <FAB
        icon="plus"
        style={styles.fab}
        onPress={onCreatePress}
        testID={'add-btn'}
      />
    </Container>
  );
};

export default React.memo(HomeScreen);
