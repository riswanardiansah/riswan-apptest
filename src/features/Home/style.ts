import {colors} from '../../common/constants';
import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: colors.gray.default,
    opacity: 0.7,
  },
  fab: {position: 'absolute', margin: 16, right: 0, bottom: 0},
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  odd: {
    backgroundColor: colors.blue.lighter,
  },
  even: {
    backgroundColor: colors.white.default,
  },
});

export default styles;
