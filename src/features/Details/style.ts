import {colors} from '../../common/constants';
import {Dimensions, StyleSheet} from 'react-native';
import {aspectRatio} from '../../common/utils';
const {width, height} = Dimensions.get('window');
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white.default,
    flexDirection: 'column',
  },
  cover: {
    flex: 2,
    height: aspectRatio(height / 3),
  },
  card: {
    marginTop: 20,
    paddingHorizontal: 20,
    flex: 3,
  },
  action: {
    // flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: 50,
    justifyContent: 'space-evenly',
  },
  separator: {height: 10},
  imageHeader: {
    width: width,
    height: '100%',
    // transform: [{scale: 0.55}],
  },
  avatarWrapper: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    width: 150,
    height: 150,
    borderRadius: 150 / 2,
    overflow: 'hidden',
    elevation: 5,
    shadowOffset: {width: -2, height: 4},
    shadowColor: colors.black.shadow,
    shadowOpacity: 0.2,
    shadowRadius: 3,
    backgroundColor: colors.white.default,
  },
});

export default style;
