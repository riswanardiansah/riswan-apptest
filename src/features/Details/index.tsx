import React, {useEffect, useCallback, useState} from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import {NavigationProps} from 'common/interface';
import {useAppDispatch, useTypedSelector} from '../../common/hooks/';
import {Contact, ContactState} from 'common/models';
import {
  deleteContactAction,
  getContactByIDAction,
} from '../../common/reducers/contact';
import {
  Button,
  Portal,
  Dialog,
  Paragraph,
  Card,
  DataTable,
  Divider,
} from 'react-native-paper';
import {Container, Text} from '../../common/components';
import style from './style';
import {validateImageUri} from '../../common/utils';

type DetailScreenProps = NavigationProps<'Details'>;

const DetailsScreen = ({navigation, route}: DetailScreenProps) => {
  const [visible, setVisible] = useState(false);

  const {selectedContact} = useTypedSelector<ContactState>('contacts');
  const getContactByID = useAppDispatch<Contact>(getContactByIDAction);
  const deleteContact = useAppDispatch<Contact>(deleteContactAction);

  useEffect(() => {
    getContactByID({id: route.params.userID});
  }, [route, getContactByID]);

  const onEditPress = useCallback(() => {
    navigation.navigate('Create', {mode: 'UPDATE'});
  }, [navigation]);

  const handleVisibleDialog = useCallback(() => {
    setVisible(!visible);
  }, [visible]);

  const onDelete = useCallback(() => {
    handleVisibleDialog();
    deleteContact({
      id: selectedContact?.id,
    });
  }, [deleteContact, selectedContact, handleVisibleDialog]);

  const renderImgUri = useCallback((uri?: string) => {
    return validateImageUri(uri);
  }, []);
  return (
    <Container style={style.container}>
      <View style={style.cover}>
        <Image
          source={
            renderImgUri(selectedContact?.photo)
              ? {uri: selectedContact?.photo}
              : require('../../common/assests/images/avatar.png')
          }
          style={style.imageHeader}
          resizeMode="cover"
          blurRadius={4}
        />
        <View style={style.avatarWrapper}>
          <Image
            source={
              renderImgUri(selectedContact?.photo)
                ? {uri: selectedContact?.photo}
                : require('../../common/assests/images/avatar.png')
            }
            style={style.avatar}
            resizeMode="cover"
          />
        </View>
      </View>
      <View style={style.card}>
        <Card elevation={5} mode="elevated">
          <Card.Content>
            <DataTable>
              <DataTable.Row>
                <DataTable.Cell>First Name</DataTable.Cell>
                <DataTable.Cell testID="firstName-detail">
                  {selectedContact?.firstName}
                </DataTable.Cell>
              </DataTable.Row>

              <DataTable.Row>
                <DataTable.Cell>Last Name</DataTable.Cell>
                <DataTable.Cell testID="lastName-detail">
                  {selectedContact?.lastName}
                </DataTable.Cell>
              </DataTable.Row>

              <DataTable.Row>
                <DataTable.Cell>Age</DataTable.Cell>
                <DataTable.Cell testID="age-detail">
                  {selectedContact?.age}
                </DataTable.Cell>
              </DataTable.Row>
            </DataTable>
          </Card.Content>
        </Card>
      </View>
      <View style={style.action}>
        <TouchableOpacity onPress={handleVisibleDialog}>
          <Text>DELETE</Text>
        </TouchableOpacity>
        <Divider />
        <TouchableOpacity onPress={onEditPress}>
          <Text>EDIT</Text>
        </TouchableOpacity>
      </View>
      <Portal>
        <Dialog visible={visible} onDismiss={handleVisibleDialog}>
          <Dialog.Title>
            Delete {selectedContact?.firstName} {selectedContact?.lastName}
          </Dialog.Title>
          <Dialog.Content>
            <Paragraph>Are you sure to delete this contact?</Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={handleVisibleDialog}>Cancel</Button>
            <Button onPress={onDelete}>REMOVE</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </Container>
  );
};

export default React.memo(DetailsScreen);
