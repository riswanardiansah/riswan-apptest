import React, {useMemo} from 'react';
import {SafeAreaView, Text, Pressable, StyleSheet} from 'react-native';
import {IconButton, Modal, ActivityIndicator} from 'react-native-paper';

type ImagePickModalProps = {
  isVisible: any;
  onClose: any;
  onImageLibraryPress: any;
  onCameraPress: any;
  loading?: boolean;
};

export const ImagePickerModal = ({
  isVisible,
  onClose,
  onImageLibraryPress,
  onCameraPress,
  loading,
}: ImagePickModalProps) => {
  const renderContent = useMemo(() => {
    if (loading) {
      return (
        <ActivityIndicator
          style={styles.indicator}
          animating={true}
          color="blue"
        />
      );
    }
    return (
      <SafeAreaView style={styles.buttons}>
        <Pressable style={styles.button} onPress={onImageLibraryPress}>
          <IconButton icon="image" size={20} />
          <Text style={styles.buttonText}>Library</Text>
        </Pressable>
        <Pressable style={styles.button} onPress={onCameraPress}>
          <IconButton icon="camera" size={20} />
          <Text style={styles.buttonText}>Camera</Text>
        </Pressable>
      </SafeAreaView>
    );
  }, [loading, onCameraPress, onImageLibraryPress]);

  return (
    <Modal
      visible={isVisible}
      onDismiss={loading ? undefined : () => onClose()}
      style={loading ? styles.modalCenter : styles.modal}>
      {renderContent}
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'flex-end',
  },
  modalCenter: {
    justifyContent: 'center',
  },
  buttonIcon: {
    width: 30,
    margin: 10,
  },
  buttons: {
    backgroundColor: 'white',
    flexDirection: 'row',
    borderRadius: 30,
    marginHorizontal: 20,
    paddingVertical: 10,
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 14,
    fontWeight: '600',
  },
  indicator: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    flex: 1,
    bottom: 0,
  },
});

export default ImagePickerModal;
