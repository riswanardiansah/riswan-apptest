import {colors} from '../../common/constants';
import {StyleSheet} from 'react-native';
const style = StyleSheet.create({
  container: {flex: 1, padding: 20, backgroundColor: colors.white.default},
  separator: {height: 10},
  photoWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    overflow: 'hidden',
  },
  iconUpload: {
    position: 'absolute',
    right: -10,
    bottom: -10,
  },
});

export default style;
