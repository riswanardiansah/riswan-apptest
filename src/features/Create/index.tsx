import React, {useState, useCallback, useMemo, useEffect} from 'react';
import {useAppDispatch, useTypedSelector} from '../../common/hooks';
import {Contact, ContactState} from 'common/models';
import {NavigationProps} from 'common/interface';
import {
  addContactAction,
  updateContactAction,
} from '../../common/reducers/contact';
import {FlatList, View, Image, Platform} from 'react-native';
import {Button, IconButton, Portal, TextInput} from 'react-native-paper';
import {Container} from '../../common/components';
import style from './style';
import ImagePickerModal from './ImagePickerModal';
import {MediaType} from 'react-native-image-picker';
import * as ImagePicker from 'react-native-image-picker';
import {validateImageUri} from '../../common/utils';

type CreateScreenProps = NavigationProps<'Create'>;

type ValueType = {
  name: string;
  label: string;
  value: string;
};

const UPLOAD_URL =
  'https://freeimage.host/api/1/upload?key=6d207e02198a847aa98d0a2a901485a5&format=json';

const initialValues: ValueType[] = [
  {name: 'photo', label: 'photo', value: ''},
  {name: 'firstName', label: 'First Name', value: ''},
  {name: 'lastName', label: 'Last Name', value: ''},
  {name: 'age', label: 'Age', value: ''},
];

const CreateScreen = ({route}: CreateScreenProps) => {
  const [loadingUpload, setLoadingUpload] = useState(false);
  const [visible, setVisible] = useState(false);
  const [values, setValues] = useState(initialValues);

  const {loading, selectedContact} = useTypedSelector<ContactState>('contacts');
  const addContact = useAppDispatch<Contact>(addContactAction);
  const updateContact = useAppDispatch<Contact>(updateContactAction);

  const handleChange = useCallback(
    (index: number, value: ValueType) => {
      const selected = [...values];
      selected[index] = value;
      setValues(selected);
    },
    [values],
  );

  const uploadImage = useCallback(
    async (uri: string, filename: string) => {
      try {
        const form = new FormData();
        uri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        form.append('source', {
          name: filename,
          uri: uri,
          type: 'image/jpeg',
        });
        const result = await fetch(UPLOAD_URL, {
          method: 'POST',
          body: form,
        });
        const data = await result.json();
        if (data) {
          const indexInput = values.findIndex(item => item.name === 'photo');
          let selectedInput = values.find(item => item.name === 'photo');
          if (selectedInput) {
            selectedInput.value = data.image.url;
            handleChange(indexInput, selectedInput);
            setLoadingUpload(false);
            setVisible(false);
          }
        }
      } catch (error) {
        console.log(error, 'ERROR');
      }
    },
    [handleChange, values],
  );

  const formatValues = useMemo(() => {
    return values.reduce((accumulator, value) => {
      let newKey = Object.values(value)[0];
      return {...accumulator, [newKey]: value.value};
    }, {}) as Contact;
  }, [values]);

  const onSubmit = useCallback(() => {
    const payload = formatValues;
    if (route.params.mode === 'CREATE') {
      addContact({
        payload,
      });
    } else {
      updateContact({
        payload,
        id: selectedContact?.id,
      });
    }
  }, [formatValues, addContact, route, updateContact, selectedContact]);

  const disabled = useMemo(() => {
    const findEmptyValue = values.find(item => item.value === '');
    const formatted = {
      ...formatValues,
      id: selectedContact?.id,
      age: Number(selectedContact?.age || 0),
    };
    const isUpdateMode =
      route.params.mode === 'UPDATE' &&
      JSON.stringify(formatted) === JSON.stringify(selectedContact);
    return Boolean(findEmptyValue) || isUpdateMode;
  }, [values, route, selectedContact, formatValues]);

  const onImageLibraryPress = useCallback(async () => {
    const options = {
      selectionLimit: 1,
      mediaType: 'photo' as MediaType,
      includeBase64: false,
    };
    const result = await ImagePicker.launchImageLibrary(options);
    if (result.assets) {
      uploadImage(
        result?.assets[0].uri as string,
        result?.assets[0].fileName as string,
      );
      setLoadingUpload(true);
    }
  }, [uploadImage]);

  const onCameraPress = useCallback(async () => {
    const options = {
      saveToPhotos: true,
      mediaType: 'photo' as MediaType,
      includeBase64: false,
    };
    const result = await ImagePicker.launchCamera(options);
    if (result.assets) {
      uploadImage(
        result?.assets[0].uri as string,
        result?.assets[0].fileName as string,
      );
      setLoadingUpload(true);
    }
  }, [uploadImage]);

  useEffect(() => {
    if (
      selectedContact &&
      JSON.stringify(selectedContact) !== '{}' &&
      route.params.mode === 'UPDATE'
    ) {
      const editValues = initialValues.map(item => {
        const value =
          selectedContact[item.name as keyof typeof selectedContact];
        return {...item, value: value.toString()};
      }) as ValueType[];
      setValues(editValues);
    }
  }, [selectedContact, route]);

  const renderImgUri = useCallback((item: string) => {
    return validateImageUri(item);
  }, []);

  return (
    <Container style={style.container}>
      <FlatList
        data={values}
        renderItem={({item, index}) => {
          if (item.name === 'photo') {
            return (
              <View style={style.photoWrapper}>
                <View>
                  <Image
                    style={style.avatar}
                    source={
                      renderImgUri(item.value)
                        ? {uri: item.value}
                        : require('../../common/assests/images/avatar.png')
                    }
                  />
                  <IconButton
                    icon="camera"
                    size={20}
                    onPress={() => setVisible(true)}
                    style={style.iconUpload}
                  />
                </View>
              </View>
            );
          }
          return (
            <View key={item.name}>
              <TextInput
                keyboardType={item.name === 'age' ? 'number-pad' : 'default'}
                label={item.label}
                mode="outlined"
                value={item.value}
                onChangeText={text =>
                  handleChange(index, {
                    ...item,
                    value:
                      item.name !== 'age'
                        ? text.replace(' ', '')
                        : text.replace(/[^0-9]/g, ''),
                  })
                }
                testID={item.name}
              />
            </View>
          );
        }}
        keyExtractor={item => item.name}
        ItemSeparatorComponent={() => <View style={style.separator} />}
      />
      <Button
        mode="contained"
        onPress={() => onSubmit()}
        dark
        disabled={disabled}
        loading={loading}>
        DONE
      </Button>
      <Portal>
        <ImagePickerModal
          isVisible={visible}
          onClose={() => setVisible(false)}
          onImageLibraryPress={onImageLibraryPress}
          onCameraPress={onCameraPress}
          loading={loadingUpload}
        />
      </Portal>
    </Container>
  );
};

export default React.memo(CreateScreen);
