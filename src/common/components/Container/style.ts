import {colors} from '../../constants';
import {StyleSheet} from 'react-native';
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white.default,
  },
});

export default style;
