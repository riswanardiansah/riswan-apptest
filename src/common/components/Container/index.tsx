import {View, ViewProps} from 'react-native';
import React, {PropsWithChildren} from 'react';
import style from './style';

type ContainerProps = PropsWithChildren & ViewProps;

const Container = ({children, ...props}: ContainerProps) => {
  return (
    <View style={[style.container, props.style]} {...props}>
      {children}
    </View>
  );
};

export default React.memo(Container);
