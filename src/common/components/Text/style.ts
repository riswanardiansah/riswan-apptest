import {StyleSheet} from 'react-native';
import {aspectRatio} from '../../utils';
import {colors} from '../../constants';
const style = StyleSheet.create({
  light: {
    color: colors.white.default,
  },
  dark: {
    color: colors.black.default,
  },
});
export const size = {
  xs: {
    fontSize: aspectRatio(11),
  },
  sm: {
    fontSize: aspectRatio(12),
  },
  md: {
    fontSize: aspectRatio(14),
  },
  lg: {
    fontSize: aspectRatio(16),
  },
  xl: {
    fontSize: aspectRatio(18),
  },
  xxl: {
    fontSize: aspectRatio(24),
  },
  xxxl: {
    fontSize: aspectRatio(28),
  },
  bigXl: {
    fontSize: aspectRatio(32),
  },
};

export default style;
