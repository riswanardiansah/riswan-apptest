import React, {PropsWithChildren} from 'react';
import {Text as DefaultText, TextProps, Appearance} from 'react-native';
import style, {size} from './style';

type CustomTextProps = PropsWithChildren &
  TextProps & {
    type?: keyof typeof size;
  };

const Text = ({type = 'md', children, ...props}: CustomTextProps) => {
  return (
    <DefaultText
      style={[
        props.style,
        {...size[type]},
        Appearance.getColorScheme() === 'dark' ? style.dark : style.light,
      ]}>
      {children}
    </DefaultText>
  );
};

export default React.memo(Text);
