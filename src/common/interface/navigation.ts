import {NativeStackScreenProps} from '@react-navigation/native-stack';

export type NavigationProps<T extends keyof RootStackParamList> =
  NativeStackScreenProps<RootStackParamList, T>;
export type RootStackParamList = {
  Home: undefined;
  Details: {
    userID?: string;
  };
  Create: {
    mode: 'CREATE' | 'UPDATE';
  };
};
