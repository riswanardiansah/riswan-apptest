export interface ResponseType<T> {
  message: string;
  data?: T;
}

export type RequestOptionGenericType<T> = {
  payload?: T;
  id?: string;
};
