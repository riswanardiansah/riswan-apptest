export interface ContactState {
  contacts: ContactDetail[];
  selectedContact?: ContactDetail;
  loading: boolean;
  error: string;
}
export interface ContactDetail extends Contact {
  id: string;
}

export interface Contact {
  firstName: string;
  lastName: string;
  age: number;
  photo: string;
}
