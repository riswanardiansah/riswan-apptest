export const colors = {
  white: {
    default: '#fff',
    light: '#FAFAFA',
  },
  gray: {
    default: '#ddd',
  },
  black: {
    shadow: '#171717',
    dark: '#1A1A1A',
    default: '#000000',
  },
  blue: {
    lighter: '#D7E9F7',
  },
};
