import {ResponseType} from 'common/interface';

const BASE_URL = 'https://simple-contact-crud.herokuapp.com/';
type Option = {
  endpoint: string;
  payload?: any;
  method: 'POST' | 'GET' | 'DELETE' | 'PATCH' | 'PUT';
  baseUrl?: string;
};

export const apiCall = async <T = unknown>({
  endpoint,
  method,
  payload,
}: Option): Promise<ResponseType<T>> => {
  try {
    const url = BASE_URL + endpoint;
    const headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    };
    const response = await fetch(url, {
      method: method,
      headers,
      body: (method !== 'GET' && JSON.stringify(payload)) || null,
    });
    const data = await response.json();
    if (!response.ok) {
      return Promise.reject(data);
    }
    return data;
  } catch (error) {
    throw new Error(error as any);
  }
};
