export const validateImageUri = (uri?: string) => {
  if (uri && uri.match(/^https.*\.(?:gif|jpg|jpeg|tiff|png)/)) {
    return true;
  }
  return false;
};
