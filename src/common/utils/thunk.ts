import {createAsyncThunk, ThunkDispatch} from '@reduxjs/toolkit';
import {RequestOptionGenericType} from 'common/interface';
import {apiCall} from './api';
import {RootState} from '../../app/store';

type ThunkUtilsType = {
  type: string;
  queryParam?: Record<any, any>;
  method: 'GET' | 'POST' | 'PUT' | 'DELETE';
  onSuccess?: (param: {
    response: unknown;
    dispatch: ThunkDispatch<any, any, any>;
    state?: any;
  }) => void;
  onFailed?: (param: {
    error: any;
    dispatch: ThunkDispatch<any, any, any>;
    payload?: any;
  }) => void;
  endpoint: string;
  payload?: Record<any, any>;
};

export const thunkUtils = <T, K = void>({
  type,
  method,
  onSuccess,
  onFailed,
  endpoint,
}: ThunkUtilsType) => {
  return createAsyncThunk(
    type,
    async (payload: RequestOptionGenericType<K>, thunkAPI) => {
      try {
        const response = await apiCall<T>({
          endpoint: endpoint + (payload.id ? `/${payload.id}` : ''),
          method,
          payload: payload.payload,
        });
        if (onSuccess) {
          onSuccess({
            response,
            dispatch: thunkAPI.dispatch,
            state: thunkAPI.getState(),
          });
        }
        return {
          ...response,
          ...payload,
          otherState: thunkAPI.getState() as RootState,
        };
      } catch (error) {
        if (onFailed) {
          onFailed({
            error,
            dispatch: thunkAPI.dispatch,
            payload: payload,
          });
        }
        return thunkAPI.rejectWithValue(error);
      }
    },
  );
};
