import {createSlice, isAnyOf} from '@reduxjs/toolkit';

import {ContactDetail, ContactState} from 'common/models';
import {
  getContactAction,
  getContactByIDAction,
  addContactAction,
  updateContactAction,
  deleteContactAction,
} from './contactThunk';

const initialState: ContactState = {
  contacts: [],
  selectedContact: {} as ContactDetail,
  loading: false,
  error: '',
};

export const contactSlice = createSlice({
  name: 'contacts',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(getContactAction.fulfilled, (state, action) => {
      const listContact = action.payload.data as ContactDetail[];
      const deletedContact =
        action.payload.otherState.deletedContact.deletedContact;
      const filteredContact = listContact?.filter(item => {
        return deletedContact.indexOf(item.id) === -1;
      });
      state.loading = false;
      state.contacts = filteredContact;
      state.selectedContact = {} as ContactDetail;
    });
    builder.addCase(addContactAction.fulfilled, state => {
      state.loading = false;
    });
    builder.addMatcher(
      isAnyOf(getContactByIDAction.fulfilled, updateContactAction.fulfilled),
      (state, action) => {
        state.loading = false;
        state.selectedContact = action.payload.data as ContactDetail;
      },
    );
    builder.addMatcher(
      isAnyOf(
        getContactAction.pending,
        getContactByIDAction.pending,
        addContactAction.pending,
        deleteContactAction.pending,
      ),
      state => {
        state.loading = true;
        state.error = initialState.error;
      },
    );
    builder.addMatcher(
      isAnyOf(
        getContactAction.rejected,
        getContactByIDAction.rejected,
        addContactAction.rejected,
        deleteContactAction.rejected,
      ),
      (state, action) => {
        state.loading = false;
        state.error = action.payload as string;
      },
    );
  },
});

export const {} = contactSlice.actions;
