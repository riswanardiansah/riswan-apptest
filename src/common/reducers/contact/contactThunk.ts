import {ContactDetail, Contact} from 'common/models';
import {thunkUtils} from '../../utils';
import {goBack} from '../../../common/navigations/rootNavigation';
import {addDeletedContact} from '../deleted-contact';
import Toast from 'react-native-simple-toast';

export const getContactAction = thunkUtils<ContactDetail[], Contact>({
  type: 'contact/getContact',
  method: 'GET',
  endpoint: 'contact',
});
export const getContactByIDAction = thunkUtils<ContactDetail, Contact>({
  type: 'contact/getContactByID',
  method: 'GET',
  endpoint: 'contact',
});
export const addContactAction = thunkUtils<Contact>({
  type: 'contact/addContact',
  endpoint: 'contact',
  method: 'POST',
  onSuccess: ({dispatch}) => {
    goBack();
    Toast.show('Successfully created');
    return dispatch(getContactAction({}));
  },
});

export const updateContactAction = thunkUtils<ContactDetail, Contact>({
  type: 'contact/updateContact',
  endpoint: 'contact',
  method: 'PUT',
  onSuccess: ({}) => {
    goBack();
    Toast.show('Successfully updated');
  },
});

export const deleteContactAction = thunkUtils<ContactDetail, Contact>({
  type: 'contact/deleteContact',
  endpoint: 'contact',
  method: 'DELETE',
  onSuccess: ({dispatch}) => {
    goBack();
    Toast.show('Successfully deleted');
    return dispatch(getContactAction({}));
  },
  onFailed: ({dispatch, payload, error}) => {
    goBack();
    Toast.show(`Error ${error.message}, but removed on local`);
    return dispatch(addDeletedContact(payload.id));
  },
});
