import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  deletedContact: [] as string[],
};

export const deletedContactSlice = createSlice({
  name: 'deletedContact',
  initialState,
  reducers: {
    addDeletedContact: (state, action) => {
      const listDeletedContact = [...state.deletedContact];
      const findDeletedContact = listDeletedContact.find(
        item => item === action.payload,
      );
      if (!findDeletedContact) {
        state.deletedContact.push(action.payload);
      }
    },
  },
});

export const {addDeletedContact} = deletedContactSlice.actions;
