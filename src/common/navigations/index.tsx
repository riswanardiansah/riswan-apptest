import React, {useMemo} from 'react';
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from '@react-navigation/native';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {HomeScreen, DetailsScreen, CreateScreen} from '../../features';
import {navigationRef} from './rootNavigation';
import {ColorSchemeName} from 'react-native';
import {useTypedSelector} from '../hooks';
import {ContactState} from '../models';
import {Text} from '../components';
import {colors} from '../constants';

type NavigationContainerProps = {
  scheme: ColorSchemeName;
};

const Stack = createNativeStackNavigator();

const Navigations = ({scheme}: NavigationContainerProps) => {
  const {selectedContact} = useTypedSelector<ContactState>('contacts');

  const renderTitle = useMemo(() => {
    const isCreateScreen = JSON.stringify(selectedContact) === '{}';
    return {
      title: isCreateScreen ? 'Create' : 'Edit',
      fontStyle: {
        color: scheme === 'dark' ? colors.white.default : colors.black.default,
        fontWeight: '600',
      } as any,
    };
  }, [selectedContact, scheme]);
  return (
    <NavigationContainer
      ref={navigationRef}
      theme={scheme === 'dark' ? DarkTheme : DefaultTheme}>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Details" component={DetailsScreen} />
        <Stack.Screen
          name="Create"
          component={CreateScreen}
          options={{
            headerTitle: () => (
              <Text
                testID="header-title"
                type="md"
                style={{...renderTitle.fontStyle}}>
                {renderTitle.title}
              </Text>
            ),
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default React.memo(Navigations);
