import {combineReducers} from 'redux';
import {
  Action,
  ThunkDispatch,
  configureStore,
  PreloadedState,
} from '@reduxjs/toolkit';
import {contactSlice} from '../common/reducers/contact';
import {deletedContactSlice} from '../common/reducers/deleted-contact';
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
const persistConfig = {
  key: 'root',
  version: 1,
  storage: AsyncStorage,
  whitelist: [deletedContactSlice.name],
};

const rootReducer = combineReducers({
  [contactSlice.name]: contactSlice.reducer,
  [deletedContactSlice.name]: deletedContactSlice.reducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const middlewares = () => {
  const middleware = [];
  if (__DEV__) {
    const createDebugger = require('redux-flipper').default;
    middleware.push(createDebugger());
  }

  return middleware;
};

const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(middlewares()),
});

const persistor = persistStore(store);

export const setupStore = (preloadedState?: PreloadedState<RootState>) => {
  return configureStore({
    reducer: rootReducer,
    preloadedState,
  });
};

export type AppDispatch = typeof store.dispatch;
export type AppStore = ReturnType<typeof setupStore>;
export type RootState = ReturnType<typeof store.getState>;
export type ThunkAppDispatch = ThunkDispatch<RootState, void, Action>;

export {store, persistor};
