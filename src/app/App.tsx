import React from 'react';
import {Provider as PaperProvider, DefaultTheme} from 'react-native-paper';
import {Provider as StoreProvider} from 'react-redux';
import Navigations from '../common/navigations';
import {store, persistor} from './store';
import {PersistGate} from 'redux-persist/integration/react';
import {useColorScheme} from 'react-native';
import {colors} from '../common/constants';

const darkTheme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.black.dark,
    accent: colors.white.light,
  },
};

const lightTheme = {
  ...DefaultTheme,
};
const App = () => {
  const scheme = useColorScheme();

  return (
    <StoreProvider store={store}>
      <PersistGate persistor={persistor}>
        <PaperProvider theme={scheme === 'dark' ? darkTheme : lightTheme}>
          <Navigations scheme={scheme} />
        </PaperProvider>
      </PersistGate>
    </StoreProvider>
  );
};

export default App;
