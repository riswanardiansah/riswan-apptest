/* eslint-disable no-undef */
jest.mock('react-native-simple-toast', () => ({
  SHORT: jest.fn(),
}));

jest.mock('redux-persist', () => {
  const real = jest.requireActual('redux-persist');
  return {
    ...real,
    persistReducer: jest
      .fn()
      .mockImplementation((config, reducers) => reducers),
  };
});
jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');
